from distutils.core import setup
from Cython.Build import cythonize

setup(
    ext_modules = cythonize("sphericalgrid_xdmf.pyx", gdb_debug=True),
    requires=['numpy', 'lxml', 'h5py', 'cython'],
)

#stdlib imports
from glob import glob
from io import StringIO

# external module imports
from lxml import etree
from lxml.etree import ElementTree, Element, Comment
import h5py

# scientific stack imports
import numpy as np

# xshells imports
import shtns
from pyxshells import load_field, get_field_info, PolTor, Grid

# local imports
import sphericalgrid_xdmf as sphgrd

def snapgen(flist, ris, sht, grid):
    """Generator of spatial data for each snapshot.

    Parameters
    ----------
    flist : iterable
        list of snapshots to return data from
    ris : ndarray (int)
        list of radial indices
    sht : shtns.sht  
        pre-initialized shtns object
    grid : pyxshells.Grid
        pre-initiailzed grid object. This speeds up the loading of fields

    Returns
    -------
    it : iterator

    Notes
    -----
    The first time the generator is called it yields the initialized
    spatial data arrays (before any calculations have been made). This
    is so that calling functions can properly declare the arrays/hdf5
    file objects that will receive the data. If new outputs are desired,
    the output arrays should be declared at the beginning and overwritten
    in place for each snapshot. 
    """
    # at each time step the data will be stored 
    # in the same memory location
    assert (len(ris) != 3), 'vector/scalar determined by first dimension being 3 or not'
    u_ts = np.empty((3, len(ris), sht.nlat, sht.nphi), np.double)
    omega = np.empty((len(ris), sht.nlat, sht.nphi), np.double)
    outs = {'vel': u_ts, 'omega': omega}
    # an initial yield is used to allocate data to the hdf5 file
    yield None, outs
    for fname in flist:
        field = load_field(fname, sht=sht, grid=grid)
        for j, ir in enumerate(ris):
            sht.SHqst_to_spat(field.rad(ir), field.sph(ir), field.tor(ir), *u_ts[:, j])
            omega[j] = u_ts[2, j] / (grid.r[ir] * np.sqrt(1.0 - sht.cos_theta**2))[:, np.newaxis]
        yield field.time, outs

def gridwrite(gridname, times, grid, sht, ris):
    """Writes a grid information file

    Parameters
    ----------
    gridname : string
        name of grid file to write (must end with '.h5')
    times : 1darry(float)
        time values of measurments
    sht : shtns.sht  
        pre-initialized shtns object
    grid : pyxshells.Grid
        pre-initiailzed grid object. This speeds up the loading of fields
    ris : 1darray(int)
        indices of radial grid points

    Returns
    -------
    npoints : int
        number of points in grid
    sphcoords : tuple (cost, sint, cosp, sinp)
        spherical coordinates of gridpoints, for use with sphgrd.vel_cart

    Notes
    -----
    """
    assert gridname.endswith('.h5'), 'grid must be written to an hdf5 file'
    # generate the spherical grid
    r = grid.r[ris]
    nr = r.size
    nth = sht.nlat
    nph = sht.nphi
    cost = sht.cos_theta
    sint = np.sqrt(1.0 - cost**2)
    phi = np.linspace(0, np.pi*2, sht.nphi+1)[:-1]
    cosp = np.cos(phi)
    sinp = np.sin(phi)
    indices = np.empty((nr, nth, nph+1), np.int64)
    # generate the cartesian grid
    xyz = np.empty((indices.size, 3), np.double)
    npoints = sphgrd.gridpoints(r, cost, sint, cosp, sinp,
                              xyz, indices, a=1.0, b=1.0, c=1.0)
    xyz = xyz[:npoints, :]

    # get the number of cells and of vertices
    if r[0] == 0.0:
        spec = np.array(((2 * nph, 5),
                         ((nth-3) * nph, 6),
                         ((nr-2) * 2 * nph, 7),
                         ((nr-2) * (nth-3) * nph, 9)))
    else:
        spec = np.array((((nr-1) * 2 * nph, 7),
                         ((nr-1) * (nth-3) * nph, 9)))
    ncells = np.sum(spec[:, 0])
    # calculate how many points we need in the topology array
    ntop = np.sum(np.prod(spec, axis=-1))
    topology = np.empty(ntop, np.int64)
    sphgrd.topology(nr, nth, nph,
                    indices, topology,
                    np.int(r[0] == 0.0))
    # write to the grid file
    with h5py.File(gridname, 'w') as h5:
        h5['time'] = np.array(times).astype(np.single)
        h5['xyz'] = xyz.astype(np.single)
        h5['topology'] = topology.astype(np.int32)
        h5['ncells'] = ncells
    return npoints, (cost, sint, cosp, sinp)

def datawrite(dataname, flist, rstride=10, gridname='grid.h5'):
    """Writes a grid information file

    Parameters
    ----------
    dataname : string
        name of data file to write (must end with '.h5')
    flist : list
        list of files to process the data of
    sht : shtns.sht  
        pre-initialized shtns object
    grid : pyxshells.Grid
        pre-initiailzed grid object. This speeds up the loading of fields
    rstride : int
        steps between gridpoints. Helps reduce side of grid
    gridname : string
        name of grid file to write (must end with '.h5')
    """
    assert dataname.endswith('.h5'), 'data must be written to a .h5 file'
    
    # get the time of each snapshot
    times = np.array([get_field_info(f, disp=False)[0]['time']
                      for f in flist])
    # initialize the sht and grid objects
    info, r = get_field_info(flist[0], disp=False)
    sht = shtns.sht(info['lmax'], info['mmax'], info['mres'])
    sht.set_grid(flags=shtns.sht_reg_poles)
    irs, ire = info['ir']
    nr = ire-irs+1
    ris = irs+np.r_[0, np.arange(1, nr-1, rstride), nr-1]

    grid = Grid(r)
    # build the unstructured grid
    npoints, sphcoords = gridwrite(gridname, times, grid, sht, ris)
    # intialize cartesian arrays
    fcart = np.empty((npoints, 3), np.double)
    scart = np.empty(npoints, np.double)
    with h5py.File(dataname, 'w') as h5:
        # initialize the snapshot generator to get the array sizes
        gener = snapgen(flist, ris, sht, grid)
        time, out = next(gener)
        h5dat = {}
        # intialize the data labels and define the chunk sizes
        for key, val in out.items():
            if len(val) == 3:
                h5dat[key] = h5.create_dataset(key, (0, npoints, 3), np.single,
                                               maxshape=(None, npoints, 3),
                                               chunks=(1, npoints, 3))
            else:
                h5dat[key] = h5.create_dataset(key, (0, npoints), np.single,
                                               maxshape=(None, npoints),
                                               chunks=(1, npoints))
        for time, out in gener:
            for key, val in out.items():
                if len(val) == 3:
                    sphgrd.cart_vel(val[0], val[1], val[2],
                                    fcart, *sphcoords)
                    h5dat[key].resize(h5dat[key].shape[0]+1, axis=0)
                    h5dat[key][-1] = fcart.astype(np.single)
                else:
                    sphgrd.cart_sca(val, scart)
                    h5dat[key].resize(h5dat[key].shape[0]+1, axis=0)
                    h5dat[key][-1] = scart.astype(np.single)                

def xdmfbuild(datafile, gridfile):
    """Builds the xdmf file from the data and grid files

    Parameters
    ----------
    datafile : string
        name of datafile (must be in h5 form)
    gridfile : string
        name of gridfile (must be in h5 form)

    Returns
    -------
    out : lxml etree object

    """
    # initialize the tree
    data = StringIO('''<Xdmf xmlns:xi="http://www.w3.org/2003/XInclude" Version="2.2"></Xdmf>''')
    out = etree.parse(data)
    out.xinclude()
    root = out.getroot()
    root.append(Comment('define the grid arrays'))
    with h5py.File(gridfile) as h5:
        elem = Element('DataItem', Name='Points', Format='HDF', NumberType='Float',
                      Dimensions='{} {}'.format(*h5['xyz'].shape))
        elem.text = '{}:/{}'.format(gridfile, 'xyz')
        root.append(elem)
        elem = Element('DataItem', Name='Topol', Format='HDF', NumberType='Int',
                      Dimensions='{}'.format(*h5['topology'].shape))
        elem.text = '{}:/{}'.format(gridfile, 'topology')
        root.append(elem)
        elem = Element('DataItem', Name='time', Format='HDF', NumberType='Float',
                      Dimensions='{}'.format(*h5['time'].shape))
        elem.text = '{}:/{}'.format(gridfile, 'time')
        root.append(elem)
        ntime = h5['time'].shape[0]

    root.append(Comment('define the data arrays'))
    with h5py.File(datafile) as h5:
        refstrs = {}
        dimstrs = {}
        attrkwargs = {}
        indarrkwargs = {}
        for key in h5.keys():
            elem = Element('DataItem', Name=key, Format='HDF', NumberType='Float',
                           Dimensions=' '.join(str(d) for d in h5[key].shape))
            elem.text = '{}:/{}'.format(datafile, key)
            root.append(elem)
            # the strings for defining hyperslabs
            refstr = ' '.join(['{} ' + ' '.join(str(0) for i in h5[key].shape[1:]),
                               ' '.join(str(1) for i in h5[key].shape),
                               '1 ' + ' '.join(str(i) for i in h5[key].shape[1:]),])
            refstrs[key] = refstr
            dimstrs[key] = ' '.join(['1', ]+list(str(i) for i in h5[key].shape[1:]))
            # the hyperslabs are defined slightly differently for vectors and scalars. 
            # use the shape of the data entry to choose parameters
            attrkwargs[key] = {'Name': key, 'Center': 'Node', 'ItemType': 'HyperSlab', 'Type': 'HyperSlab'}
            if len(h5[key].shape) == 2:
                attrkwargs[key].update(AttributeType='Scalar')
                indarrkwargs[key] = {'Name': key, 'Dimensions': '2 3', 'Format': 'XML'}
            if len(h5[key].shape) == 3:
                attrkwargs[key].update(AttributeType='Vector')
                indarrkwargs[key] = {'Name': key, 'Dimensions': '3 3', 'Format': 'XML'}

    root.append(Comment('define the grid objects'))
    # get the grid information
    geometry = Element('Geometry', Name='GridGeom', GeometryType='XYZ')
    geometry.append(Element('DataItem', Reference=r"/Xdmf/DataItem[@Name='Points']"))
    with h5py.File(gridfile) as h5:
        topology = Element('Topology', Name='GridTopol',
                           NumberOfElements=str(h5['ncells'].value), TopologyType='Mixed')
        topology.append(Element('DataItem', Reference=r"/Xdmf/DataItem[@Name='Topol']"))

    root.append(geometry)
    root.append(topology)
    domain = Element('Domain')
    # define the grid collection of the time series
    tseries = Element('Grid', Name='potato', GridType='Collection', CollectionType='Temporal')
    domain.append(tseries)
    timeelem = Element('Time', TimeType='List')
    timeelem.append(Element('DataItem', Reference=r"/Xdmf/DataItem[@Name='time']"))
    tseries.append(timeelem)
    root.append(Comment('define the time series'))
    # run through each time step and generate the grid definition
    for i in range(ntime):
        grid = Element('Grid', Name='potato {}'.format(i), GridType='Uniform')
        tseries.append(grid)
        grid.append(Element('Geometry', Reference=r"/Xdmf/Geometry[@Name='GridGeom']"))
        grid.append(Element('Topology', Reference=r"/Xdmf/Topology[@Name='GridTopol']"))
        for key in refstrs.keys():
            att = Element('Attribute', **attrkwargs[key])
            hyper = Element('DataItem', ItemType="HyperSlab", Dimensions=dimstrs[key],
                            Type='HyperSlab')
            hyparr = Element('DataItem', **indarrkwargs[key])
            hyparr.text = refstrs[key].format(i)
            hyper.append(hyparr)
            hyper.append(Element('DataItem', Reference=r"/Xdmf/DataItem[@Name='{}']".format(key)))
            att.append(hyper)
            grid.append(att)
    root.append(domain)
    return out    
    

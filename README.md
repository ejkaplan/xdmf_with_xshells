# XDMF file format and `Paraview`

The purpose of this tutorial is to show how to use the [XDMF][1] file
format and HDF5 as a way of getting data out of an xshells simulation
and into `paraview` in a way that is memory efficient and allows as much
functionality as possible within `paraview`. Part of the reason is that
I've seen two paradigms in the group for generating files for use with
`paraview`. There's the method built into `xspp` which generates a
separate xdmf file for each file that's processed. The problem with
this is if a series of snapshots are processed, `Paraview` won't be able
to step through the snapshots and update whatever visualization is
shown. The other is to generate a separate `.vtu` file for each
snapshot. On the plus side, `Paraview` knows how to step through these;
on the down side this has the problem of wasting a lot of disk space
in defining the same grid for each timestep. If only a small amount of
data is being written each time then more bits are being spilt on a
needless redundancy than on the data actually being plotted.

## This tutorial
The plan is to demonstrate how to generate a time series of spatial
data in a form that can be used with `Paraview` from a series of
snapshots output by `xshells`. The requirements are the `pyxshells`
utilities that come with xshells, the `h5py` module, and the `lxml`
module. The latter two should both be available through `$ pip install`. This tutorial comes with a cython routine to generate the grid, the cell topology, and convert the spatial data output by `PolTor.spat_shell` to values at gridpoints, and a set of xshells field files as example data. Running 

     $ python3 setup.py build_ext --inplace 

at the command line in the directory should build the sphericalgrid_xdmf.so shared object needed for the operations here

## Preliminaries
The xdmf format relies on breaking the dataset up into two components, the data and the model. For the purposes of this tutorial we'll be storing the data in hdf5 format, but binary and ascii formats are also options. The full specification of the format is available [here][1], the discussion here will be limited to things I've 1) found useful and 2) figured out well enough to actually implement

[1]: http://www.xdmf.org/index.php/XDMF_Model_and_Format
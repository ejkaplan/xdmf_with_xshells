cimport numpy as np

ctypedef np.double_t DTYPE_T
ctypedef np.int64_t ITYPE_T

def gridpoints(np.ndarray[DTYPE_T, ndim=1] r,
               np.ndarray[DTYPE_T, ndim=1] cost,
               np.ndarray[DTYPE_T, ndim=1] sint,
               np.ndarray[DTYPE_T, ndim=1] cosp,
               np.ndarray[DTYPE_T, ndim=1] sinp, 
               np.ndarray[DTYPE_T, ndim=2] xyz,
               np.ndarray[ITYPE_T, ndim=3] indices,
               np.float a=1.0, np.float b=1.0, np.float c=1.0):
    # indices
    cdef int i, j, k, ii
    # array bounds
    cdef int nr = r.shape[0]
    cdef int nth = cost.shape[0]
    cdef int nph = cosp.shape[0]
    ii = 0
    i = 0
    # treat origin separately if it is in the grid
    if (r[i] == 0.0):
        j = 0
        k = 0
        indices[i, j, k] = ii
        xyz[ii, 0] = 0.0
        xyz[ii, 1] = 0.0
        xyz[ii, 2] = 0.0
        ii += 1
        i += 1

    for i in range(i, nr):
        # theta = 0
        j = 0
        k = 0
        indices[i, j, k] = ii
        xyz[ii, 0] = 0.0
        xyz[ii, 1] = 0.0
        xyz[ii, 2] = r[i] * cost[j] * c
        ii += 1
        for j in range(1, nth-1):
            for k in range(0, nph):
                indices[i, j, k] = ii
                xyz[ii, 0] = r[i] * sint[j] * cosp[k] * a
                xyz[ii, 1] = r[i] * sint[j] * sinp[k] * b
                xyz[ii, 2] = r[i] * cost[j] * c
                ii += 1
            # add an extra point for wrapping
            indices[i, j, k+1] = indices[i, j, 0]
        # theta = 0
        j = nth-1
        k = 0
        indices[i, j, k] = ii
        xyz[ii, 0] = 0.0
        xyz[ii, 1] = 0.0
        xyz[ii, 2] = r[i] * cost[j] * c
        ii += 1
    return ii

def cart_vel(np.ndarray[DTYPE_T, ndim=3] vr,
             np.ndarray[DTYPE_T, ndim=3] vt,
             np.ndarray[DTYPE_T, ndim=3] vp,
             np.ndarray[DTYPE_T, ndim=2] vcart,
             np.ndarray[DTYPE_T, ndim=1] cost,
             np.ndarray[DTYPE_T, ndim=1] sint,
             np.ndarray[DTYPE_T, ndim=1] cosp,
             np.ndarray[DTYPE_T, ndim=1] sinp,
             np.int origin=0):
    # indices
    cdef int i, j, k, ii
    # array bounds
    cdef int nr = vr.shape[0]
    cdef int nth = vr.shape[1]
    cdef int nph = vr.shape[2]
    # cylindrical velocity
    cdef DTYPE_T vs
    ii = 0
    i = 0

    # set Vs to zero at origin if necessary
    if (origin):
        vcart[ii, 0] = 0.0
        vcart[ii, 1] = 0.0
        vcart[ii, 2] = 0.0
        ii += 1
        i += 1
    for i in range(i, nr):
        # theta = 0
        j = 0
        k = 0
        vs = vr[i, j, k] * sint[j] + vt[i, j, k] * cost[j]
        vcart[ii, 0] = 0.0
        vcart[ii, 1] = 0.0
        vcart[ii, 2] = -vr[i, 0, 0]
        ii += 1
        # theta in (0, pi)
        for j in range(1, nth-1):
            for k in range(0, nph):
                vs = vr[i, j, k] * sint[j] + vt[i, j, k] * cost[j]
                vcart[ii, 0] = vs * cosp[k] - vp[i, j, k] * sinp[k]
                vcart[ii, 1] = vs * sinp[k] + vp[i, j, k] * cosp[k]
                vcart[ii, 2] = vr[i, j, k] * cost[j] - vt[i, j, k] * sint[j]
                ii += 1
        # theta = pi
        vcart[ii, 0] = 0.0
        vcart[ii, 1] = 0.0
        vcart[ii, 2] = vr[i, 0, 0]
        ii += 1
    return


def cart_sca(np.ndarray[DTYPE_T, ndim=3] s,
             np.ndarray[DTYPE_T, ndim=1] scart,
             np.int origin=0):
    # indices
    cdef int i, j, k, ii
    # array bounds
    cdef int nr = s.shape[0]
    cdef int nth = s.shape[1]
    cdef int nph = s.shape[2]
    # cylindrical velocity
    cdef DTYPE_T vs
    ii = 0
    i = 0

    # set Vs to zero at origin if necessary
    if (origin):
        ii += 1
        i += 1
    for i in range(i, nr):
        # theta = 0
        j = 0
        k = 0
        scart[ii] = s[i, 0, 0]
        ii += 1
        # theta in (0, pi)
        for j in range(1, nth-1):
            for k in range(0, nph):
                scart[ii] = s[i, j, k]
                ii += 1
        # theta = pi
        scart[ii] = s[i, 0, 0]
        ii += 1
    return


def topology(np.int nr, np.int nth, np.int nph,
             np.ndarray[ITYPE_T, ndim=3] indices,
             np.ndarray[ITYPE_T, ndim=1] topology,
		 np.int origin=0):
    cdef int i, j, k, ii
    cdef int tetra, pyrim, wedge, hexa

    tetra = 6
    pyram = 7
    wedge = 8
    hexa = 9

    ii = 0
    i = 0

    # treat origin separately if it is in the grid
    if (origin):
        j = 0
        # theta = 0, tetrahedron
        for k in range(nph):
            topology[ii] = tetra; ii += 1
            topology[ii] = indices[i, j, 0]; ii += 1
            topology[ii] = indices[i+1, j, 0]; ii += 1
            topology[ii] = indices[i+1, j+1, k+1]; ii += 1
            topology[ii] = indices[i+1, j+1, k]; ii += 1
        # theta in (0, pi), pyramid:
        for j in range(1, nth-2):
            for k in range(nph):
                topology[ii] = pyram; ii += 1
                topology[ii] = indices[i+1, j, k]; ii += 1
                topology[ii] = indices[i+1, j, k+1]; ii += 1
                topology[ii] = indices[i+1, j+1, k+1]; ii += 1
                topology[ii] = indices[i+1, j+1, k]; ii += 1
                topology[ii] = indices[i, 0, 0]; ii += 1
        # theta = pi, tetrahedron
        j = nth-2
        for k in range(nph):
            topology[ii] = tetra; ii += 1
            topology[ii] = indices[i+1, j+1, k]; ii += 1
            topology[ii] = indices[i+1, j+1, k+1]; ii += 1
            topology[ii] = indices[i+1, j, 0]; ii += 1
            topology[ii] = indices[i, j, 0]; ii += 1
        i += 1

    for i in range(i, nr-1):
        # theta = 0, wedge
        j = 0
        for k in range(nph):
            topology[ii] = wedge; ii += 1
            topology[ii] = indices[i, j, 0]; ii += 1
            topology[ii] = indices[i, j+1, k+1]; ii += 1
            topology[ii] = indices[i, j+1, k]; ii += 1
            topology[ii] = indices[i+1, j, 0]; ii += 1
            topology[ii] = indices[i+1, j+1, k+1]; ii += 1
            topology[ii] = indices[i+1, j+1, k]; ii += 1
            # theta in (0, pi), hexahedron
        for j in range(1, nth-2):
            for k in range(nph):
                topology[ii] = hexa; ii += 1
                topology[ii] = indices[i, j, k]; ii += 1
                topology[ii] = indices[i, j, k+1]; ii += 1
                topology[ii] = indices[i, j+1, k+1]; ii += 1
                topology[ii] = indices[i, j+1, k]; ii += 1
                topology[ii] = indices[i+1, j, k]; ii += 1
                topology[ii] = indices[i+1, j, k+1]; ii += 1
                topology[ii] = indices[i+1, j+1, k+1]; ii += 1
                topology[ii] = indices[i+1, j+1, k]; ii += 1
        # theta = pi, wedge
        j = nth-2
        for k in range(nph):
            topology[ii] = wedge; ii += 1
            topology[ii] = indices[i, j, k]; ii += 1
            topology[ii] = indices[i, j, k+1]; ii += 1
            topology[ii] = indices[i, j+1, 0]; ii += 1
            topology[ii] = indices[i+1, j, k]; ii += 1
            topology[ii] = indices[i+1, j, k+1]; ii += 1
            topology[ii] = indices[i+1, j+1, 0]; ii += 1
    return ii


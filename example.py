from glob import glob
from xshells_xdmf import datawrite, xdmfbuild

if __name__=='__main__':
    flist = sorted(glob('fieldU_*'))
    datawrite('example.h5', flist)
    xml = xdmfbuild('example.h5', 'grid.h5')
    xml.write('example.xdmf', xml_declaration=True, encoding='us-ascii', pretty_print=True)
